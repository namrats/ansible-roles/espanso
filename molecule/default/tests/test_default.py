import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_espanso(host):
    cmd = host.run('bash -l -c \'which espanso\'')
    assert cmd.rc == 0
