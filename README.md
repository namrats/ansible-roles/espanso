# Espanso Ansible role

## Setting up development environment

Requires [Pipenv][pipenv] and [Docker][docker].

- Create the virtualenv and install development dependencies.
  ```shell
  pipenv install --dev
  ```

## Running the tests

```shell
pipenv run molecule test --all
```

## Test Driven Development cycles

- Create the testing environment
  ```shell
  pipenv run molecule create -s <scenario>
  pipenv run molecule converge -s <scenario>
  ```

### Red cap 🔴

- Modify files in `molecule/<scenario>`, especially `tests/test_default.py`
- Run the test scenario and make sure it fails
  ```shell
  pipenv run molecule verify -s <scenario>
  ```

### Green cap ✔️

- Modify files in the role, especially `tasks/main.yml`
- Apply the role then run the test scenario to make sure it succeeds
  ```shell
  pipenv run molecule converge -s <scenario>
  pipenv run molecule verify -s <scenario>
  ```

### Blue cap 🔵

- Take a step back and look at your code. What can make it better, clearer?
- Use the linters to help
  ```shell
  pipenv run molecule lint -s <scenario>
  ```
- Make sure you didn't break anything.
  ```shell
  pipenv run molecule test --all
  ```

[pipenv]:https://pipenv.kennethreitz.org/en/latest/
[docker]:https://docker.com
